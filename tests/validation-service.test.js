const validationService = require('../services/validation-service');

const mockRequest = {
  origin: {
    lon: 14.473298,
    lat: 50.103528
  },
  destination: {
    lon: 14.421834,
    lat: 50.082627
  },
  time: 800,
  waypoints: [
    {
      name: "Tesnovsky tunel",
      lon: 14.432316,
      lat: 50.093532
    },
    {
      name: "Konevova",
      lon: 14.467979,
      lat: 50.090336
    },
    {
      name: "Narodni divadlo",
      lon: 14.413022,
      lat: 50.081386
    }
  ]
};

test('test empty payload', ()=> {
  expect(validationService.validate_payload(null)).toHaveProperty('valid', false);
  expect(validationService.validate_payload(undefined)).toHaveProperty('valid', false);
  expect(validationService.validate_payload([])).toHaveProperty('valid', false);
  expect(validationService.validate_payload({})).toHaveProperty('valid', false);
});

function flawed_coordinate(other, node_name) {
  let r = [];
  r.push([]);
  r.push({});
  r.push({...other, lat: '', lon: 15});
  r.push({...other, lat: -100, lon: 15});
  r.push({...other, lat: 20, lon: 200});
  r.push({...other, lat: 100, lon: 15});
  r.push({...other, lat: 20, lon: -200});
  r.push({...other, lat: 5, lon: ''});
  if (node_name) {
    return r.map(v => {
      let s = {};
      s[node_name] = v;
      return s;
    });
  } else {
    return r;
  }
}
test('test flawed payloads', ()=> {
  let flaws = [];
  flaws.push({origin: true}, {destination: true}, {time: true}, {waypoints: true});
  flaws.push(...flawed_coordinate({}, 'origin'));
  flaws.push(...flawed_coordinate({}, 'destination'));
  flaws.push({time: -5}, {time: undefined}, {time: []});
  flaws.push({waypoints: []}, {waypoints: {}}, {waypoints: ''}, {waypoints: 5});
  for (let i = 0; i < flaws.length; i++) {
    let flaw = flaws[i];
    let payload = {...mockRequest, ...flaw};
    expect(validationService.validate_payload(payload))
      .toHaveProperty('valid', false);
  }
  let waypoint_flaws = [];
  waypoint_flaws.push(...flawed_coordinate({name: 'A'}).map(o => {
    return {waypoints: [o]};
  }));
  waypoint_flaws.push({waypoints: [{lat: 0, lon: 0, name: []}]});
  waypoint_flaws.push({waypoints: [{lat: 0, lon: 0, name: {}}]});
  waypoint_flaws.push({waypoints: [{lat: 0, lon: 0}]});
  waypoint_flaws.push({waypoints: [{lat: 0, lon: 0, name: true}]});
  for (let j = 0; j < waypoint_flaws.length; j++) {
    let flaw = waypoint_flaws[j];
    let payload = {...mockRequest, ...flaw};
    expect(validationService.validate_payload(payload))
      .toHaveProperty('valid', false);
  }
});

test('test valid payloads', ()=> {
  expect(validationService.validate_payload(mockRequest)).toHaveProperty('valid', true);
});


