const rewire = require('rewire');

const routeService = rewire('../services/route-service');

function testDistanceCalculation() {
  const calculate_distance = routeService.__get__('calculate_distance');

  test('distance between the same points should be 0', () => {
    expect(calculate_distance([15, 50], {lat: 50, lon: 15})).toBe(0);
  });

  test('distance between different points should greater than 0', () => {
    expect(calculate_distance([15, 50], {lat: 55, lon: 15})).toBeGreaterThan(0);
  });
}

function replaceDistanceWithEuclidian() {
  routeService.__set__('calculate_distance', function (point, destination) {
    return Math.sqrt(Math.pow(point[1] - destination.lat, 2) + Math.pow(point[0] - destination.lon, 2));
  });

  const calculate_distance = routeService.__get__('calculate_distance');
  test('testing euclidian distance', () => {
    expect(calculate_distance([12, 54], {lat: 50, lon: 15})).toBe(5);
  })
}

function testDistanceAvgCalculation() {
  const calculate_distance = routeService.__get__('calculate_distance');
  const calculate_distance_avg = routeService.__get__('calculate_distance_avg');

  test('distance avg 0%', () => {
    expect(calculate_distance_avg([15, 50], [15, 55], 0, {
      lat: 55,
      lon: 20
    })).toBe(calculate_distance([15, 50], {lat: 55, lon: 20}));
  });

  test('distance avg 100%', () => {
    expect(calculate_distance_avg([15, 50], [15, 55], 1, {
      lat: 55,
      lon: 20
    })).toBe(calculate_distance([15, 55], {lat: 55, lon: 20}));
  });
}

function testSearchTime() {
  const search_time = routeService.__get__('search_time');
  test('search time - target = 0', () => {
    expect(search_time(0, 0, [1, 1, 1])).toEqual({
      time_passed: true,
      index_before: -1,
      time_before: 0,
      time_after: 0
    });
  });

  test('search time - target = too much', () => {
    expect(search_time(0, 10, [1, 1, 1])).toEqual({
      time_passed: false,
      index_before: 2,
      time_before: 2,
      time_after: 3
    });
  });

  test('search time - target = during the route', () => {
    expect(search_time(0, 5, [1, 2, 3])).toEqual({
      time_passed: true,
      index_before: 2,
      time_before: 3,
      time_after: 6
    });
  });

  test('search time - target = end of the route', () => {
    expect(search_time(0, 6, [1, 2, 3])).toEqual({
      time_passed: true,
      index_before: 2,
      time_before: 3,
      time_after: 6
    });
  });
}

function testSimulation() {
  const simulate = routeService.__get__('simulate');
  const mockRouteStraight = { //this may be extracted to JSON file
    durations: [1, 1, 3],
    geometry: {
      coordinates: [[0, 0], [1, 0], [2, 0], [5, 0]]
    }
  };
  test('simulate straight route', () => {
    expect(simulate({time: 3, destination: {lat: 0, lon: 5}}, mockRouteStraight)).toHaveProperty('distance', 2);
  });
  test('simulate straight route time = 0', () => {
    expect(simulate({time: 0, destination: {lat: 0, lon: 5}}, mockRouteStraight)).toHaveProperty('distance', 5);
  });
  test('simulate straight route time = too much', () => {
    expect(simulate({time: 10, destination: {lat: 0, lon: 5}}, mockRouteStraight)).toHaveProperty('distance', 0);
  });

  const mockRouteCurved = { //this may be extracted to JSON file
    durations: [1, 1, 3, 5, 1, 5],
    geometry: {
      coordinates: [[0, 0], [1, 0], [2, 0], [5, 0], [10, 0], [10, 1], [5, 1]]
    }
  };
  test('simulate curved route time = close drive by', () => {
    expect(simulate({time: 5, destination: {lat: 1, lon: 5}}, mockRouteCurved)).toHaveProperty('distance', 1);
  });
  test('simulate curved route time = almost farthest', () => {
    expect(simulate({time: 11, destination: {lat: 1, lon: 5}}, mockRouteCurved)).toHaveProperty('distance', 5);
  });
}

function testDelayCalculation() {
  const calculate_delay = routeService.__get__('calculate_delay');

  test('calculate delay - ride faster then target time', () => {
    expect(calculate_delay({}, 0, {ride: {time_passed: false}})).toBe(0);
  });

  const mockCalculation = (time_after, index_before) => {
    return {
      original_result: {
        geometry: {
          coordinates: [[0, 0], [2, 0], [5, 0], [8, 0], [10, 0]]
        },
        durations: [2, 3, 3, 2]
      },
      ride: {
        time_passed: true,
        time_after: time_after,
        index_before: index_before
      }
    }
  };
  const mockDestination = {
    lat: 0,
    lon: 10
  };
  test('calculate delay', () => {
    routeService.__set__('negativeDelayAllowed', true);
    expect(calculate_delay({destination: mockDestination, time: 1}, 6, mockCalculation())).toBe(3);
  });
  test('calculate delay - 0 delay', () => {
    routeService.__set__('negativeDelayAllowed', true);
    expect(calculate_delay({destination: mockDestination, time: 0}, 10, mockCalculation())).toBe(0);
  });
  test('calculate delay - full delay', () => {
    routeService.__set__('negativeDelayAllowed', true);
    expect(calculate_delay({destination: mockDestination, time: 0}, 0, mockCalculation())).toBe(10);
  });
  test('calculate delay - last segment delay', () => {
    routeService.__set__('negativeDelayAllowed', true);
    expect(calculate_delay({destination: mockDestination, time: 0}, 1, mockCalculation())).toBe(9);
  });
  test('calculate delay [negative delay not allowed]', () => {
    routeService.__set__('negativeDelayAllowed', false);
    expect(calculate_delay({destination: mockDestination, time: 1}, 6, mockCalculation(2, 0))).toBe(3);
  });
  test('calculate delay - 0 delay [negative delay not allowed]', () => {
    routeService.__set__('negativeDelayAllowed', false);
    expect(calculate_delay({destination: mockDestination, time: 0}, 10, mockCalculation(0, -1))).toBe(0);
  });
  test('calculate delay - full delay [negative delay not allowed]', () => {
    routeService.__set__('negativeDelayAllowed', false);
    expect(calculate_delay({destination: mockDestination, time: 0}, 0, mockCalculation(0, -1))).toBe(10);
  });
  test('calculate delay - last segment delay [negative delay not allowed]', () => {
    routeService.__set__('negativeDelayAllowed', false);
    expect(calculate_delay({destination: mockDestination, time: 0}, 1, mockCalculation(0, -1))).toBe(9);
  });

  const mockCalculationNeg = (time_after, index_before) => {
    return {
      original_result: {
        geometry: {
          coordinates: [[0, 0], [2, 0], [5, 0], [8, 0], [10, 0], [10, 1], [5, 1]]
        },
        durations: [2, 3, 3, 2, 1, 5]
      },
      ride: {
        time_passed: true,
        time_after: time_after,
        index_before: index_before
      }
    }
  };
  const mockDestinationNeg = {
    lat: 1,
    lon: 5
  };
  test('calculate delay - proper negative delay test', () => {
    routeService.__set__('negativeDelayAllowed', false);
    expect(calculate_delay({destination: mockDestinationNeg, time: 10}, 1, mockCalculationNeg(10, 3))).toBe(5);
  });
  test('calculate delay - proper negative delay test [negative delay not allowed]', () => {
    routeService.__set__('negativeDelayAllowed', true);
    expect(calculate_delay({destination: mockDestinationNeg, time: 10}, 1, mockCalculationNeg(10, 3))).toBe(-5);
  });
}

function testWinnerChoosing() {
  const calculate_delays = routeService.__get__('calculate_delays');
  const choose_winner_and_delays = routeService.__get__('choose_winner_and_delays');

  const mockWaypoints = [{
    name: "A",
    lat: 0,
    lon: 5
  }, {
    name: "B",
    lat: 5,
    lon: 0
  }];

  test('check winner A', ()=> {
    routeService.__set__('calculate_delays', () => { //empty mock for easier test
    });
    expect(choose_winner_and_delays({waypoints: mockWaypoints}, [{distance: 1}, {distance: 2}])).toHaveProperty("winnerName", "A");
    routeService.__set__('calculate_delays', calculate_delays); //replace mock with original function
  });

  test('check winner B', ()=> {
    routeService.__set__('calculate_delays', () => { //empty mock for easier test
    });
    expect(choose_winner_and_delays({waypoints: mockWaypoints}, [{distance: 10}, {distance: 2}])).toHaveProperty("winnerName", "B");
    routeService.__set__('calculate_delays', calculate_delays); //replace mock with original function
  });
}

function testFullFunctionality() {
  //todo
}

testDistanceCalculation();
replaceDistanceWithEuclidian();
testDistanceAvgCalculation();
testSearchTime();
testSimulation();
testDelayCalculation();
testWinnerChoosing();
testFullFunctionality();