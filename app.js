const express = require('express');
const bodyParser = require('body-parser');
const authorization = require('./services/authorization-service');
const route_service = require('./services/route-service');
const validation_service = require('./services/validation-service');

const app = express();

const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const SERVER_ERROR = 500;

app.use(bodyParser.json());

app.all('*', (req, res, next) => {
  if (req.headers['x-secret'] === undefined || !authorization.authorize(req.headers['x-secret'])) {
    res.sendStatus(UNAUTHORIZED);
  } else {
    next();
  }
});

app.post('/', (req, res) => {
  var validation_result = validation_service.validate_payload(req.body);
  if (validation_result.valid) {
    route_service.winner(req.body)
      .then(result => res.send(result))
      .catch(err => {
        console.log("Error while calculating: " + err);
        res.sendStatus(SERVER_ERROR);
      });
  } else {
    console.log('Invalid request:', req.body, validation_result.messages);
    res.sendStatus(BAD_REQUEST);
  }
});

app.listen(3000);