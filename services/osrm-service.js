const config = require('./../config/default.json');
const OSRM = require("osrm.js");

const osrm = new OSRM(config.osrm.url);

function get_route(coordinates) {
  return new Promise((resolve, reject) => {
    osrm.route({
        coordinates: coordinates,
        alternatives: false,
        overview: 'full',
        geometries: 'geojson',
        annotations: 'duration'//distance
      }, (err, result) => {
        if (err || !result.routes || result.routes.length === 0) {
          reject(err, result);
        } else {
          resolve(result.routes[0]);
        }
      }
    );

  });
}

module.exports = {
  get_route: get_route
};