const db = require('./db-service');

function authorize(secret) {
  return (secret === db.getSecret()); //we should probably use some caching of the secret if we expect a lot of subsequent API calls
}

module.exports = {
  authorize: authorize
};