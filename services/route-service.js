const geolib = require('geolib');
const osrm_service = require('./osrm-service');
const config = require('./../config/default.json');

var negativeDelayAllowed = config.negativeDelayAllowed;

/**
 * Averages two coordinates based on percentage of time elapsed/time total
 */
function calculate_distance_avg(point1, point2, percentage, destination) {
  let point = [
    point1[0] + percentage * (point2[0] - point1[0]),
    point1[1] + percentage * (point2[1] - point1[1])
  ];
  return calculate_distance(point, destination);
}

/**
 * Calculates the distance between two points using geolib library
 */
function calculate_distance(point, destination) {
  return geolib.getDistance({
    latitude: point[1],
    longitude: point[0]
  }, {
    latitude: destination.lat,
    longitude: destination.lon
  }, config.distancePrecision);
}

function search_time(start_time, target_time, durations) {
  let intermediary_time = start_time;
  let i = -1;
  do {
    if (intermediary_time >= target_time) {
      break;
    } else {
      i++;
      intermediary_time += durations[i];
    }
  } while (i < durations.length - 1);
  return {
    time_passed: (intermediary_time >= target_time),
    index_before: i,
    time_before: intermediary_time - (durations[i] ? durations[i] : 0),
    time_after: intermediary_time
  }
}
/**
 * Car ride simulation
 */
function simulate(params, result) {
  let ride = search_time(0, params.time, result.durations);
  let distance;
  var coordinates = result.geometry.coordinates;
  if (ride.time_passed) { //if specified time has passed during the ride
    const index_after = ride.index_before + 1;
    const index_before = Math.max(ride.index_before, 0);
    let percentage = 0;
    if (ride.time_after > ride.time_before) { //evade DIV by 0
      percentage = (params.time - ride.time_before) / (ride.time_after - ride.time_before);
    }
    distance = calculate_distance_avg(coordinates[index_before], coordinates[index_after], percentage, params.destination);
  } else {
    distance = 0; //ride ended before specified time
  }
  return {
    original_result: result,
    distance: distance,
    ride: ride
  };
}

/**
 * Simulates one segment of the route in config.time-precision steps to find out when does the car reach the desired distance from the destination. The segment is approximated as a straight line.
 */
function simulate_segment(point1, point2, destination, travel_time, target_distance) {
  let time_passed = 0;
  while (time_passed <= travel_time) {
    let point = [point1[0] + time_passed / travel_time * (point2[0] - point1[0]), point1[1] + time_passed / travel_time * (point2[1] - point1[1])];
    var calculateDistance = calculate_distance(point, destination);
    if (calculateDistance <= target_distance) {
      return time_passed;
    }
    time_passed += config.timePrecision;
  }
  return travel_time; //in case the distance is reached in the last of the "timePrecision" sized segments
}

function calculate_delay(params, winner_distance, calculation) {
  if (calculation.ride.time_passed) {
    let initial_index = 0;
    let intermediary_time = 0;
    if (!negativeDelayAllowed) {
      //if negative delays are not allowed, we can start checking only after the time specified
      intermediary_time = calculation.ride.time_after;
      initial_index = calculation.ride.index_before + 1;
    }
    let coordinates = calculation.original_result.geometry.coordinates;
    for (let i = initial_index; i < coordinates.length; i++) {
      if (calculate_distance(coordinates[i], params.destination) <= winner_distance) {
        var last_travel_time = calculation.original_result.durations[i - 1] ? calculation.original_result.durations[i - 1] : 0;
        var last_segment_partial_time = simulate_segment(coordinates[i == 0 ? 0 : i - 1],
          coordinates[i],
          params.destination,
          last_travel_time,
          winner_distance);
        return intermediary_time - last_travel_time + last_segment_partial_time - params.time;
      }
      const segment_duration = calculation.original_result.durations[i] ? calculation.original_result.durations[i] : 0;
      intermediary_time += segment_duration; //durations array is shorter by 1 than coordinates
    }
  } else {
    return 0;
  }
}

/**
 * Calculates delays of other cars
 */
function calculate_delays(params, winner_index, winner_distance, calculations) {
  let delays = {};
  for (let i = 0; i < calculations.length; i++) {
    if (i === winner_index) {
      delays[params.waypoints[i].name] = 0;
    } else {
      delays[params.waypoints[i].name] = calculate_delay(params, winner_distance, calculations[i]);
    }
  }
  return delays;
}

/**
 * Calculates the values that get returned to caller
 */
function choose_winner_and_delays(params, calculations) {
  let winner_distance;
  let winner_index = 0;
  for (let i = 0; i < calculations.length; i++) {
    if (!winner_distance || calculations[i].distance < winner_distance) {
      winner_distance = calculations[i].distance;
      winner_index = i;
    }
  }
  let winner_name = params.waypoints[winner_index].name;
  return {
    winnerName: winner_name,
    winnerDistance: winner_distance,
    delays: calculate_delays(params, winner_index, winner_distance, calculations)
  }
}

function transform_coor(coordinate) {
  return [coordinate.lon, coordinate.lat];
}

function winner(params) {
  let promises = [];
  for (let i = 0; i < params.waypoints.length; i++) {
    promises[i] = osrm_service.get_route([transform_coor(params.origin), transform_coor(params.waypoints[i]), transform_coor(params.destination)])
      .then(result => {
        result.durations = result.legs[0].annotation.duration.concat(result.legs[1].annotation.duration); //only 2 legs because of 1 waypoint
        return simulate(params, result)
      });
  }
  return Promise.all(promises).then(function (calculations) {
    return choose_winner_and_delays(params, calculations);
  });
}

module.exports = {
  winner: winner
};