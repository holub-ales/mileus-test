function check_coordinate(coordinate, node_name, messages) {
  let check_value = (value, max) => value && (typeof value === 'number' && Math.abs(value) <= max);
  if (typeof coordinate === 'object' && check_value(coordinate.lat, 90) && check_value(coordinate.lon, 180)) {
    return true;
  } else {
    messages.push(node_name + '.invalid');
    return false;
  }
}

function check_time(time, messages) {
  if (!(time && typeof time === 'number' && time >= 0)) {
    messages.push('time.invalid');
  }
}

function check_waypoints(waypoints, messages) {
  if (waypoints && typeof waypoints === 'object' && Array.isArray(waypoints) && waypoints.length > 0) {
    for (let i = 0; i < waypoints.length; i++) {
      if (check_coordinate(waypoints[i], 'waypoint.'+i, messages)) {
        if (!(waypoints[i].name && typeof waypoints[i].name === 'string' && waypoints[i].name.length > 0)) {
          messages.push('waypoint.' + i + '.name.invalid');
        }
      }
    }
  } else {
    messages.push('waypoints.invalid');
  }
}

function validate_payload(payload) {
  let messages = [];
  if (payload && typeof payload === 'object') {
    check_coordinate(payload.origin, 'origin', messages);
    check_coordinate(payload.destination, 'destination', messages);
    check_time(payload.time, messages);
    check_waypoints(payload.waypoints, messages);
  } else {
    messages.push('payload.invalid');
  }
  return {
    valid: messages.length === 0,
    messages: messages
  }
}

module.exports = {
  validate_payload: validate_payload
};